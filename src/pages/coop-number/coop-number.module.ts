import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoopNumberPage } from './coop-number';

@NgModule({
  declarations: [
    CoopNumberPage,
  ],
  imports: [
    IonicPageModule.forChild(CoopNumberPage),
  ],
})
export class CoopNumberPageModule {}
