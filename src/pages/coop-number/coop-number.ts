import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-coop-number',
  templateUrl: 'coop-number.html',
})
export class CoopNumberPage {

	key: string;

	coop: {
		number?:number
	} = {};

	profile: {
		latitude?:number,
		longitude?:number,
	} = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,
  	private geolocation: Geolocation) {
  }

  send() {
  	this.geolocation.watchPosition().subscribe((position) => {
        this.profile.latitude = position.coords.latitude
        this.profile.longitude = position.coords.longitude
        //this.fire.updateProfile(this.key, this.profile)
  	})
  }

  /*ionViewDidLoad() {
    console.log('ionViewDidLoad CoopNumberPage');
  }*/

}
